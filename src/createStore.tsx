import React from 'react';

export interface StoreComponentProps<Value> {
    children?: React.ReactNode | ((value: Value) => React.ReactNode);
}

export interface StoreComponent<Value, Props = {}> {
    (props: Omit<Props, 'children'> & StoreComponentProps<Value>): React.ReactElement<any, any> | null;
    propTypes?: React.WeakValidationMap<Props>;
    contextTypes?: React.ValidationMap<any>;
    defaultProps?: Partial<Props>;
    displayName?: string;
}

export type UseStore<Value> = () => Value;

export type StoreTuple<Value, Props> = [StoreComponent<Value, Props>, UseStore<Value>];

export type UseStoreValue<Value, Props> = (props: Props) => Value;

export type CreateStore = <Value, Props>(
    useValue: UseStoreValue<Value, Props>,
    storeName?: string,
) => StoreTuple<Value, Props>;

export const createStore: CreateStore = (useValue, storeName) => {
    const Context = React.createContext<any>(null);
    const Store: StoreComponent<any, any> = (props) => {
        const value = useValue(props);
        return (
            <Context.Provider value={value}>
                {typeof props.children === 'function' ? props.children(value) : props.children}
            </Context.Provider>
        );
    };
    Store.displayName = storeName ? `Store(${storeName})` : 'Store';
    const useStore = () => {
        const value = React.useContext(Context);
        if (value === null) {
            throw new Error(
                `Store context is empty. Please mount ${Store.displayName} component above in the component tree.`,
            );
        }
        return value;
    };
    return [Store, useStore];
};
